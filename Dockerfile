FROM sagargadewar/selenium-test
RUN git clone https://bartoszsobkowiak@bitbucket.org/bartoszsobkowiak/selenium-test.git
WORKDIR /selenium-test/
COPY . .
RUN mvn package
