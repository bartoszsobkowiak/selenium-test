# UI Upp Test Automation Project


## How to use:

- [Installation](#installation)
- [Examples](#examples)
- [Technologies](#technologies)
- [Dependencies](#dependencies)
- [Reports](#reports)


---

## Installation
### Clone

- Clone this repository to your local machine using the command below:
```
	$ git clone https://bartoszsobkowiak@bitbucket.org/bartoszsobkowiak/selenium-test.git
```

---

### Execution

> Access project root

```
	$ cd selenium-test/
```
> Execute the command to run all tests in the project

```
	$ mvn clean test
```
> Execute the command to run only one test class in the project

```
	$ mvn clean test -Dtest=WelcomeTest
```
or
```
	$ mvn clean test -Dtest=UppBasicTest
```
---
## Technologies:
- Selenium WebDriver
- Java
- Maven

---

## Dependencies
* *[selenium](https://www.selenium.dev/)* 
* *[testng](https://testng.org/)* 
* *[extentreports](http://www.extentreports.com/)*
* *[webdrivermanager](https://github.com/bonigarcia/webdrivermanager)* 
* *[lombok](https://projectlombok.org/)*

---

## Reports
* To view report of test, access the file: */target/report/test_execution.html*

---
